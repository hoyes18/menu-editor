﻿namespace MenuCreator
{
    partial class Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonTextureList = new System.Windows.Forms.ImageList(this.components);
            this.surfaceBG = new System.Windows.Forms.PictureBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newMenuProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.loadMenuProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditOptionsGroup = new System.Windows.Forms.GroupBox();
            this.menuTitleGroup = new System.Windows.Forms.GroupBox();
            this.menuText = new System.Windows.Forms.TextBox();
            this.menuBGGroup = new System.Windows.Forms.GroupBox();
            this.menuBG = new System.Windows.Forms.PictureBox();
            this.menuBGSelector = new System.Windows.Forms.ComboBox();
            this.menuTemplateCheckBox = new System.Windows.Forms.CheckBox();
            this.menuTemplateLabel = new System.Windows.Forms.Label();
            this.buttonEditOptionsGroup = new System.Windows.Forms.GroupBox();
            this.buttonBGGroup = new System.Windows.Forms.GroupBox();
            this.buttonBG = new System.Windows.Forms.PictureBox();
            this.buttonBGSelector = new System.Windows.Forms.ComboBox();
            this.buttonTitleGroup = new System.Windows.Forms.GroupBox();
            this.buttonText = new System.Windows.Forms.TextBox();
            this.nextMenuGroup = new System.Windows.Forms.GroupBox();
            this.nextMenuText = new System.Windows.Forms.TextBox();
            this.createButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.surfaceBG)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.menuEditOptionsGroup.SuspendLayout();
            this.menuTitleGroup.SuspendLayout();
            this.menuBGGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.menuBG)).BeginInit();
            this.buttonEditOptionsGroup.SuspendLayout();
            this.buttonBGGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBG)).BeginInit();
            this.buttonTitleGroup.SuspendLayout();
            this.nextMenuGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonTextureList
            // 
            this.buttonTextureList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.buttonTextureList.ImageSize = new System.Drawing.Size(16, 16);
            this.buttonTextureList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // surfaceBG
            // 
            this.surfaceBG.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.surfaceBG.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.surfaceBG.Location = new System.Drawing.Point(0, 27);
            this.surfaceBG.Name = "surfaceBG";
            this.surfaceBG.Size = new System.Drawing.Size(812, 741);
            this.surfaceBG.TabIndex = 0;
            this.surfaceBG.TabStop = false;
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1024, 24);
            this.menuStrip.TabIndex = 20;
            this.menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.loadToolStripMenuItem1,
            this.saveToolStripMenuItem1,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newMenuProjectToolStripMenuItem,
            this.newMenuToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.newToolStripMenuItem.Text = "New";
            // 
            // newMenuProjectToolStripMenuItem
            // 
            this.newMenuProjectToolStripMenuItem.Name = "newMenuProjectToolStripMenuItem";
            this.newMenuProjectToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.newMenuProjectToolStripMenuItem.Text = "Menu Project";
            this.newMenuProjectToolStripMenuItem.Click += new System.EventHandler(this.newMenuProjectToolStripMenuItem_Click);
            // 
            // newMenuToolStripMenuItem
            // 
            this.newMenuToolStripMenuItem.Name = "newMenuToolStripMenuItem";
            this.newMenuToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.newMenuToolStripMenuItem.Text = "Menu";
            this.newMenuToolStripMenuItem.Click += new System.EventHandler(this.newMenuToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem1
            // 
            this.loadToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadMenuProjectToolStripMenuItem,
            this.loadMenuToolStripMenuItem});
            this.loadToolStripMenuItem1.Name = "loadToolStripMenuItem1";
            this.loadToolStripMenuItem1.Size = new System.Drawing.Size(100, 22);
            this.loadToolStripMenuItem1.Text = "Load";
            // 
            // loadMenuProjectToolStripMenuItem
            // 
            this.loadMenuProjectToolStripMenuItem.Name = "loadMenuProjectToolStripMenuItem";
            this.loadMenuProjectToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.loadMenuProjectToolStripMenuItem.Text = "Menu Project";
            this.loadMenuProjectToolStripMenuItem.Click += new System.EventHandler(this.loadMenuProjectToolStripMenuItem_Click);
            // 
            // loadMenuToolStripMenuItem
            // 
            this.loadMenuToolStripMenuItem.Name = "loadMenuToolStripMenuItem";
            this.loadMenuToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.loadMenuToolStripMenuItem.Text = "Menu";
            this.loadMenuToolStripMenuItem.Click += new System.EventHandler(this.loadMenuToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem1
            // 
            this.saveToolStripMenuItem1.Enabled = false;
            this.saveToolStripMenuItem1.Name = "saveToolStripMenuItem1";
            this.saveToolStripMenuItem1.Size = new System.Drawing.Size(100, 22);
            this.saveToolStripMenuItem1.Text = "Save";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(97, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // menuEditOptionsGroup
            // 
            this.menuEditOptionsGroup.Controls.Add(this.menuTitleGroup);
            this.menuEditOptionsGroup.Controls.Add(this.menuBGGroup);
            this.menuEditOptionsGroup.Controls.Add(this.menuTemplateCheckBox);
            this.menuEditOptionsGroup.Controls.Add(this.menuTemplateLabel);
            this.menuEditOptionsGroup.Location = new System.Drawing.Point(812, 59);
            this.menuEditOptionsGroup.Name = "menuEditOptionsGroup";
            this.menuEditOptionsGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.menuEditOptionsGroup.Size = new System.Drawing.Size(212, 231);
            this.menuEditOptionsGroup.TabIndex = 0;
            this.menuEditOptionsGroup.TabStop = false;
            this.menuEditOptionsGroup.Text = "Menu Edit Options";
            // 
            // menuTitleGroup
            // 
            this.menuTitleGroup.Controls.Add(this.menuText);
            this.menuTitleGroup.Location = new System.Drawing.Point(6, 19);
            this.menuTitleGroup.Name = "menuTitleGroup";
            this.menuTitleGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuTitleGroup.Size = new System.Drawing.Size(200, 59);
            this.menuTitleGroup.TabIndex = 0;
            this.menuTitleGroup.TabStop = false;
            this.menuTitleGroup.Text = "Menu Title";
            // 
            // menuText
            // 
            this.menuText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.menuText.Location = new System.Drawing.Point(9, 19);
            this.menuText.Name = "menuText";
            this.menuText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuText.Size = new System.Drawing.Size(181, 20);
            this.menuText.TabIndex = 11;
            this.menuText.TabStop = false;
            this.menuText.Text = "Enter the menu title...";
            this.menuText.Click += new System.EventHandler(this.menuText_Clicked);
            // 
            // menuBGGroup
            // 
            this.menuBGGroup.Controls.Add(this.menuBG);
            this.menuBGGroup.Controls.Add(this.menuBGSelector);
            this.menuBGGroup.Location = new System.Drawing.Point(6, 84);
            this.menuBGGroup.Name = "menuBGGroup";
            this.menuBGGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuBGGroup.Size = new System.Drawing.Size(200, 106);
            this.menuBGGroup.TabIndex = 0;
            this.menuBGGroup.TabStop = false;
            this.menuBGGroup.Text = "Menu Background";
            // 
            // menuBG
            // 
            this.menuBG.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.menuBG.BackColor = System.Drawing.Color.Gray;
            this.menuBG.Location = new System.Drawing.Point(9, 19);
            this.menuBG.Name = "menuBG";
            this.menuBG.Size = new System.Drawing.Size(78, 40);
            this.menuBG.TabIndex = 0;
            this.menuBG.TabStop = false;
            // 
            // menuBGSelector
            // 
            this.menuBGSelector.Location = new System.Drawing.Point(9, 65);
            this.menuBGSelector.Name = "menuBGSelector";
            this.menuBGSelector.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuBGSelector.Size = new System.Drawing.Size(181, 21);
            this.menuBGSelector.TabIndex = 0;
            this.menuBGSelector.TabStop = false;
            this.menuBGSelector.Text = "Select the menus background...";
            // 
            // menuTemplateCheckBox
            // 
            this.menuTemplateCheckBox.Location = new System.Drawing.Point(130, 205);
            this.menuTemplateCheckBox.Name = "menuTemplateCheckBox";
            this.menuTemplateCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuTemplateCheckBox.Size = new System.Drawing.Size(20, 20);
            this.menuTemplateCheckBox.TabIndex = 0;
            this.menuTemplateCheckBox.TabStop = false;
            this.menuTemplateCheckBox.Text = "Checkbox";
            this.menuTemplateCheckBox.CheckedChanged += new System.EventHandler(this.menuTemplateCheckBox_CheckedChanged);
            // 
            // menuTemplateLabel
            // 
            this.menuTemplateLabel.Location = new System.Drawing.Point(12, 208);
            this.menuTemplateLabel.Name = "menuTemplateLabel";
            this.menuTemplateLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuTemplateLabel.Size = new System.Drawing.Size(138, 20);
            this.menuTemplateLabel.TabIndex = 0;
            this.menuTemplateLabel.Text = "Create custom menu?";
            // 
            // buttonEditOptionsGroup
            // 
            this.buttonEditOptionsGroup.Controls.Add(this.buttonBGGroup);
            this.buttonEditOptionsGroup.Controls.Add(this.buttonTitleGroup);
            this.buttonEditOptionsGroup.Controls.Add(this.nextMenuGroup);
            this.buttonEditOptionsGroup.Location = new System.Drawing.Point(812, 324);
            this.buttonEditOptionsGroup.Name = "buttonEditOptionsGroup";
            this.buttonEditOptionsGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonEditOptionsGroup.Size = new System.Drawing.Size(212, 257);
            this.buttonEditOptionsGroup.TabIndex = 0;
            this.buttonEditOptionsGroup.TabStop = false;
            this.buttonEditOptionsGroup.Text = "Button Edit Options";
            this.buttonEditOptionsGroup.Visible = false;
            // 
            // buttonBGGroup
            // 
            this.buttonBGGroup.Controls.Add(this.buttonBG);
            this.buttonBGGroup.Controls.Add(this.buttonBGSelector);
            this.buttonBGGroup.Location = new System.Drawing.Point(6, 84);
            this.buttonBGGroup.Name = "buttonBGGroup";
            this.buttonBGGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonBGGroup.Size = new System.Drawing.Size(200, 93);
            this.buttonBGGroup.TabIndex = 0;
            this.buttonBGGroup.TabStop = false;
            this.buttonBGGroup.Text = "Button Texture";
            // 
            // buttonBG
            // 
            this.buttonBG.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBG.BackColor = System.Drawing.Color.Gray;
            this.buttonBG.Location = new System.Drawing.Point(9, 19);
            this.buttonBG.Name = "buttonBG";
            this.buttonBG.Size = new System.Drawing.Size(78, 40);
            this.buttonBG.TabIndex = 0;
            this.buttonBG.TabStop = false;
            // 
            // buttonBGSelector
            // 
            this.buttonBGSelector.Location = new System.Drawing.Point(9, 65);
            this.buttonBGSelector.Name = "buttonBGSelector";
            this.buttonBGSelector.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonBGSelector.Size = new System.Drawing.Size(181, 21);
            this.buttonBGSelector.TabIndex = 0;
            this.buttonBGSelector.TabStop = false;
            this.buttonBGSelector.Text = "Select the buttons background...";
            // 
            // buttonTitleGroup
            // 
            this.buttonTitleGroup.Controls.Add(this.buttonText);
            this.buttonTitleGroup.Location = new System.Drawing.Point(6, 19);
            this.buttonTitleGroup.Name = "buttonTitleGroup";
            this.buttonTitleGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonTitleGroup.Size = new System.Drawing.Size(200, 59);
            this.buttonTitleGroup.TabIndex = 0;
            this.buttonTitleGroup.TabStop = false;
            this.buttonTitleGroup.Text = "Button Title";
            // 
            // buttonText
            // 
            this.buttonText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.buttonText.Location = new System.Drawing.Point(9, 19);
            this.buttonText.Name = "buttonText";
            this.buttonText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonText.Size = new System.Drawing.Size(181, 20);
            this.buttonText.TabIndex = 11;
            this.buttonText.TabStop = false;
            this.buttonText.Text = "Enter the button title...";
            this.buttonText.Click += new System.EventHandler(this.buttonText_Clicked);
            // 
            // nextMenuGroup
            // 
            this.nextMenuGroup.Controls.Add(this.nextMenuText);
            this.nextMenuGroup.Location = new System.Drawing.Point(6, 183);
            this.nextMenuGroup.Name = "nextMenuGroup";
            this.nextMenuGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.nextMenuGroup.Size = new System.Drawing.Size(200, 59);
            this.nextMenuGroup.TabIndex = 0;
            this.nextMenuGroup.TabStop = false;
            this.nextMenuGroup.Text = "Next Menu";
            // 
            // nextMenuText
            // 
            this.nextMenuText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nextMenuText.Location = new System.Drawing.Point(9, 19);
            this.nextMenuText.Name = "nextMenuText";
            this.nextMenuText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.nextMenuText.Size = new System.Drawing.Size(181, 20);
            this.nextMenuText.TabIndex = 11;
            this.nextMenuText.TabStop = false;
            this.nextMenuText.Text = "Enter a menu name...";
            this.nextMenuText.Click += new System.EventHandler(this.nextMenuText_Clicked);
            // 
            // createButton
            // 
            this.createButton.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createButton.Location = new System.Drawing.Point(869, 587);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(96, 38);
            this.createButton.TabIndex = 7;
            this.createButton.Text = "Create Button!";
            this.createButton.UseVisualStyleBackColor = true;
            this.createButton.Visible = false;
            // 
            // Editor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.menuEditOptionsGroup);
            this.Controls.Add(this.createButton);
            this.Controls.Add(this.buttonEditOptionsGroup);
            this.Controls.Add(this.surfaceBG);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "Editor";
            this.Text = "Editor";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Editor_Load);
            this.Activated += new System.EventHandler(this.Editor_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.surfaceBG)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.menuEditOptionsGroup.ResumeLayout(false);
            this.menuTitleGroup.ResumeLayout(false);
            this.menuTitleGroup.PerformLayout();
            this.menuBGGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.menuBG)).EndInit();
            this.buttonEditOptionsGroup.ResumeLayout(false);
            this.buttonBGGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonBG)).EndInit();
            this.buttonTitleGroup.ResumeLayout(false);
            this.buttonTitleGroup.PerformLayout();
            this.nextMenuGroup.ResumeLayout(false);
            this.nextMenuGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

           }

        #endregion

        #region Data Members
        private System.Windows.Forms.PictureBox surfaceBG;
        
        #region Menu Strip
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newMenuProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem loadMenuProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem; 
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        #endregion

        #region Menu Editing Options
        private System.Windows.Forms.GroupBox menuEditOptionsGroup;
        private System.Windows.Forms.GroupBox menuTitleGroup;
        private System.Windows.Forms.TextBox menuText;
        private System.Windows.Forms.GroupBox menuBGGroup;
        private System.Windows.Forms.PictureBox menuBG;
        private System.Windows.Forms.ComboBox menuBGSelector;
        private System.Windows.Forms.CheckBox menuTemplateCheckBox;
        private System.Windows.Forms.Label menuTemplateLabel;
        #endregion

        #region Button Editing Options
        private System.Windows.Forms.GroupBox buttonEditOptionsGroup;
        private System.Windows.Forms.GroupBox buttonBGGroup;
        private System.Windows.Forms.PictureBox buttonBG;
        private System.Windows.Forms.ComboBox buttonBGSelector;
        private System.Windows.Forms.GroupBox buttonTitleGroup;
        private System.Windows.Forms.TextBox buttonText;
        private System.Windows.Forms.GroupBox nextMenuGroup;
        private System.Windows.Forms.TextBox nextMenuText;
        private System.Windows.Forms.Button createButton;
        #endregion

        private System.Windows.Forms.ImageList buttonTextureList;
        #endregion
        
        

    }
}

