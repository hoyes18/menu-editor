﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MenuCreator
{
    public partial class Editor : Form
    {

        public Editor()
        {
            InitializeComponent();
        }

        private void Editor_Load(object sender, EventArgs e)
        {
            
        }

        private void Editor_Activated(object sender, EventArgs e)
        {
            ActiveForm.Text = "Menu System Creator/Editor";
        }

        //
        //Exit Click
        //
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //
        // New > Menu Project
        //
        private void newMenuProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Create new menu project here
        }
        //
        // New > Menu
        //
        private void newMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Create new menu page
        }
        //
        // Load > Menu Project
        //
        private void loadMenuProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Load existing menu project
        }
        //
        // Load > Menu
        //
        private void loadMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Load existing menu
        }

        private void menuText_Clicked(object sender, EventArgs e)
        {
            this.menuText.Text = "";
        }

        private void buttonText_Clicked(object sender, EventArgs e)
        {
            this.buttonText.Text = "";
        }

        private void nextMenuText_Clicked(object sender, EventArgs e)
        {
            this.nextMenuText.Text = "";
        }

        private void menuTemplateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
           this.buttonEditOptionsGroup.Visible = this.createButton.Visible = this.menuTemplateCheckBox.Checked;
        }

    }
}
